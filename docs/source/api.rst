API
===
MauSPAF's code is mainly divided in three different parts:

* Mass management
* GUI
* Mass library

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api_mm
   api_masslib
   api_gui


