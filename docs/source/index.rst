.. MauSPAF documentation master file, created by
   sphinx-quickstart on Thu Apr  7 17:55:08 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MauSPAF's documentation!
===================================

MauSPAF (from the German **MAssen- Und SchwerPunktsAnalyse für die Flugzeugentwicklung**) is a program used to perform mass properties analysis. While originally developed for aircraft design, it can be used to manage mass properties mass, position and inertias of any technical system.

MauSPAF lays a special focus on accounting for uncertainties, allowing the user to enter probability distributions instead of a single value for both masses and positions of the aircraft's elements. With this approach, mass tracking as well as quantitavely determining insecurities and probabilities to reach target masses and CGs is made possible.

Besides the main classes `MassElement` and `MassElement3D`, and `MassObject3D`, used to describe 3D objects with mass properties (mass, center of gravity (c.g.), moments of inertia and products of inertia), tree structures can be specified. Also, a small GUI for mass and c.g. calculation is provided. To open it, install MauSPAF and execute `mauspafgui`.


.. toctree::
   :maxdepth: 4
   :caption: Contents:

   getting_started	     
   user_guide	     
   api	     

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
