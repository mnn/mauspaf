Getting started
===============

Installation
------------
Python
++++++
The simplest method to install `MauSPAF` is using `pip`. On a terminal/command prompt, just execute the following code.

.. code::

   pip install mauspaf

You can now access `mauspaf`'s functions importing the module in your python code.

.. code::

   import mauspaf
   
GUI
+++
Once the python module is installed, executing `mauspafgui` opens a simple GUI.

For Windows, an executable file bundling all necessary python libraries and the code to run the GUI can be found in `~/bin/v.X.X.X/*.exe`. 


Usage
-----
See examples provided in the :ref:`user_guide`.
