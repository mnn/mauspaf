# MauSPAF
[![pipeline status](https://gitlab.com/mnn/mauspaf/badges/master/pipeline.svg)](https://gitlab.com/mnn/mauspaf/-/commits/master) [![coverage report](https://gitlab.com/mnn/mauspaf/badges/master/coverage.svg)](https://gitlab.com/mnn/mauspaf/-/commits/master) [![Downloads](https://pepy.tech/badge/mauspaf)](https://pepy.tech/project/mauspaf) [![Downloads](https://pepy.tech/badge/mauspaf/month)](https://pepy.tech/project/mauspaf) [![PyPI version](https://badge.fury.io/py/mauspaf.svg)](https://badge.fury.io/py/mauspaf)

MauSPAF (from the German **MAssen- Und SchwerPunktsAnalyse für die Flugzeugentwicklung**) is a program used to perform mass properties analysis. While originally developed for aircraft design, it can be used to manage mass properties mass, position and inertias of any technical system.


# Documentation
To read the documentation, follow [this link](https://mnn.gitlab.io/mauspaf/).


# Installation
MauSPAF is provided through [PyPi](https://pypi.org/project/mauspaf/). Depending on the used python version, it can be installed with the command

    >> pip install mauspaf

or

    >> pip3 install mauspaf


# GUI
To start the GUI, run

    >> mauspafgui
    
in a terminal with mauspaf installed. Alternatively, users of Windows can download the executable provided in `~/bin/vX.X.X/*.exe`. The file is large (~130 Mb) because it includes all needed python libraries.

# About
This module has been developed as a side project during my PhD studies at the [Institute of Structural Mechanics and Lightweight Design at RWTH Aachen University](https://www.sla.rwth-aachen.de).
