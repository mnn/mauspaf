# -*- mode: python ; coding: utf-8 -*-
import os
import sys
import codecs
import datetime
sys.setrecursionlimit(5000)

block_cipher = None


def read(rel_path):
    here = os.getcwd()
    with codecs.open(os.path.join(here, rel_path), 'r') as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith('__version__'):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


a = Analysis(['src/mauspaf/application.py'],
             pathex=[os.getcwd() + '/venv-mauspaf/Lib/site-packages',
                     os.getcwd() + '/src'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             hooksconfig={
                 "matplotlib": {
                     "backends": "Qt5Agg"
                     }
                 },
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
          cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='mauspaf_' + get_version('./src/mauspaf/__init__.py') +
               '_windows_' + datetime.datetime.now().strftime("%Y%m%d"),
          debug=True,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False)
