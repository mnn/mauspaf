MAUSPAF
=========

MauSPAF (from the German **MAssen- Und SchwerPunktsAnalyse für die Flugzeugentwicklung**) is a program used to perform mass properties analysis. While originally developed for aircraft design, it can be used to manage mass properties mass, position and inertias of any technical system.

