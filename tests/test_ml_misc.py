import pytest
import sympy as sym
from mauspaf.masslibrary.misc import valid_inputs, status_changed, dict_conv


def test_valid_inputs():
    a, b, c, d, e, f = sym.symbols('a b c d e f')

    eq0 = ["a - b"]
    eq1 = ["a - (b+c)"]
    eq2 = ["a - (b+c)", "c - d"]
    eq3 = ["a + b - c", "a - b - d"]
    eq4 = ["a + b - c", "a - b - d", "e - f"]

    assert valid_inputs(eq0) == set(map(frozenset, [{a}, {b}]))
    assert valid_inputs(eq1) == set(map(frozenset, [{a, b}, {b, c}, {a, c}]))
    assert valid_inputs(eq2) == set(map(frozenset, [
        {a, b}, {b, c}, {a, c}, {b, d}, {a, d}]))
    assert valid_inputs(eq3) == set(map(frozenset, [
        {b, c}, {b, d}, {c, d}, {a, b}, {a, c}, {a, d}]))
    assert valid_inputs(eq4) == set(map(frozenset, [
        {b, c, e}, {b, d, e}, {c, d, e}, {a, b, e}, {a, c, e}, {a, d, e},
        {b, c, f}, {b, d, f}, {c, d, f}, {a, b, f}, {a, c, f}, {a, d, f}]))


def test_status_changed():
    a, b, c, d, e, f = sym.symbols('a b c d e f')
    eq = ["a + b - c", "a - b - d", "e - f"]
    valid_input_sets = valid_inputs(eq)
    valid_input_sets_1 = {frozenset({a}), frozenset({b})}

    current_status = {a: True, b: True, c: False, d: False, e: False, f: False}
    current_status_1 = [{a: False, b: False}, {a: True, b: True},
                        {a: True, b: True}, {a: False, b: False}]

    priority_list = [d, b, e, a, c, f]
    priority_list_1 = [[b, a], [a, b], [b, a], [a, b]]

    expected_new_status = {a: True, b: True, c: False, d: False, e: True,
                           f: False}
    expected_new_status_1 = [{a: False, b: True}, {a: False, b: True},
                             {a: True, b: False}, {a: True, b: False}]

    assert status_changed(current_status, priority_list, valid_input_sets) \
        == expected_new_status
    for stat, prio, news in zip(current_status_1, priority_list_1,
                                expected_new_status_1):
        assert status_changed(stat, prio, valid_input_sets_1) == news


def test_dict_conv():
    input_dict = {'r': 1, 'd': None}
    calc_dict = {'r': 1000, 'd': 2000}
    input_units = {'r': 'km', 'd': 'mm'}
    original_units = {'r': 'm', 'd': 'm'}

    res1 = dict_conv(input_dict, input_units, original_units, 'input')
    assert res1["r_i"] == 1000

    res2 = dict_conv(calc_dict, original_units, input_units, 'output')
    assert res2["d"] == 2e6
