import pytest
from mauspaf.core.mauspaf import MassElement, MassTree
from uncertain import UncertainValue

# Constant cg
a_mass = UncertainValue(10, 5, 15)
a_cg = UncertainValue(2)
a = MassElement(a_mass, a_cg)

b_mass = UncertainValue(10, 5, 15)
b_cg = UncertainValue(2)
b = MassElement(b_mass, b_cg,)

c = MassElement(None, None, 'c')

mmt = MassTree(
    {x.id: x for x in [a, b, c]},
    {a.id: 'c', b.id: 'c'})


# Testing functions
def test_cg():
    # Tests if the resulting cg is correct
    cg = mmt.elements['c'].cg
    assert pytest.approx(cg.upper_bound) == cg.lower_bound
