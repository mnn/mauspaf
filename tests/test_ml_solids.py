import pytest
import numpy as np

import mauspaf.masslibrary.solids as sl


def test_cylinder_full():
    r = 1
    le = 10
    rho = 2700

    # Full calculation
    cylinder_results = sl.cylinder_full(r=r, le=le, rho=rho)

    assert pytest.approx(cylinder_results['m']) == 84823.0016469244
    assert pytest.approx(cylinder_results['v']) == le*r**2*np.pi
    assert pytest.approx(cylinder_results['ix']) == 728064.0975
    assert pytest.approx(cylinder_results['iy']) == 728064.0975
    assert pytest.approx(cylinder_results['iz']) == 42411.50082
    assert pytest.approx(cylinder_results['s']) == 69.115

    # Partial calculations
    # TODO: improve calculation speed. They're too slow.
    with pytest.warns(UserWarning):
        assert sl.cylinder_full(r=r)["d"] == 2
    with pytest.warns(UserWarning):
        assert sl.cylinder_full(r=r, le=le)["d"] == 2

    # Different input & output units
    assert sl.cylinder_full(r=1, le=1, rho=1, units={'r': 'm', 'd': 'mm'}
                            )['d'] == 2e3

    # Check units output
    a = sl.cylinder_full(r=1, le=1, rho=1, units={'r': 'cm', 'd': 'mm'})
    assert 'units' in a
    assert a['units']['r'] == 'cm'
    assert a['units']['d'] == 'mm'


def test_cube():
    a = 1
    rho = 2700

    cube_results = sl.cube(a=a, rho=rho)

    assert pytest.approx(cube_results['m']) == 2700
    assert pytest.approx(cube_results['ix']) == 450
    assert pytest.approx(cube_results['iy']) == 450
    assert pytest.approx(cube_results['iz']) == 450
    assert pytest.approx(cube_results['s']) == 6

    cube_mass_1 = sl.cube(a=1, rho=2700)['m']
    cube_mass_2 = sl.cube(v=1, ix=450)['m']
    assert cube_mass_1 == cube_mass_2


def test_rectangular_prism():
    a = 1
    b = 1
    h = 1
    rho = 2700

    rectangular_prism_results = sl.rectangular_prism(a=a, b=b, h=h, rho=rho)

    assert pytest.approx(rectangular_prism_results['m']) == 2700
    assert pytest.approx(rectangular_prism_results['ix']) == 450
    assert pytest.approx(rectangular_prism_results['iy']) == 450
    assert pytest.approx(rectangular_prism_results['iz']) == 450
    assert pytest.approx(rectangular_prism_results['s']) == 6


def test_sphere():
    r = 1
    rho = 2700

    sphere_results = sl.sphere(r=r, rho=rho)

    assert pytest.approx(sphere_results['m']) == 11309.73355
    assert pytest.approx(sphere_results['ix']) == 4523.893421
    assert pytest.approx(sphere_results['iy']) == 4523.893421
    assert pytest.approx(sphere_results['iz']) == 4523.893421
    assert pytest.approx(sphere_results['s']) == 12.56637


def test_hollow_sphere():
    ro = 2
    ri = 1
    rho = 2700

    hollow_sphere_results = sl.hollow_sphere(ro=ro, ri=ri, rho=rho)

    assert pytest.approx(hollow_sphere_results['m']) == 79168.13487
    assert pytest.approx(hollow_sphere_results['ix']) == 140240.6961
    assert pytest.approx(hollow_sphere_results['iy']) == 140240.6961
    assert pytest.approx(hollow_sphere_results['iz']) == 140240.6961
    assert pytest.approx(hollow_sphere_results['s']) == 4*np.pi*5


def test_hemisphere():
    r = 1
    rho = 2700

    hemisphere_results = sl.hemisphere(r=r, rho=rho)

    assert pytest.approx(hemisphere_results['m']) == 5654.866776
    assert pytest.approx(hemisphere_results['ix']) == 1470.265362
    assert pytest.approx(hemisphere_results['iy']) == 1470.265362
    assert pytest.approx(hemisphere_results['iz']) == 2261.94671
    assert pytest.approx(hemisphere_results['s']) == 9.424777961


def test_hollow_cylinder():
    ro = 2
    ri = 1
    le = 1
    rho = 2700

    hollow_cylinder_results = sl.hollow_cylinder(ro=ro, ri=ri, le=le, rho=rho)

    assert pytest.approx(hollow_cylinder_results['m']) == 25446.90049
    assert pytest.approx(hollow_cylinder_results['ix']) == 33929.20066
    assert pytest.approx(hollow_cylinder_results['iy']) == 33929.20066
    assert pytest.approx(hollow_cylinder_results['iz']) == 63617.25123
    assert pytest.approx(hollow_cylinder_results['s']) == 37.69911184307752


def test_elliptical_cylinder():
    a = 1
    b = 2
    h = 1
    rho = 2700

    elliptical_cylinder_results = sl.elliptical_cylinder(a=a, b=b, h=h,
                                                         rho=rho)

    assert pytest.approx(elliptical_cylinder_results['m']) == 16964.60033
    assert pytest.approx(elliptical_cylinder_results['ix']) == 18378.31702
    assert pytest.approx(elliptical_cylinder_results['iy']) == 5654.866777
    assert pytest.approx(elliptical_cylinder_results['iz']) == 21205.75041
    assert pytest.approx(elliptical_cylinder_results['s']) == 9.424777961


def test_ellipsoid():
    a = 1
    b = 2
    c = 3
    rho = 2700

    ellipsoid_results = sl.ellipsoid(a=a, b=b, c=c, rho=rho)

    assert pytest.approx(ellipsoid_results['m']) == 67858.40132
    assert pytest.approx(ellipsoid_results['ix']) == 135716.8026
    assert pytest.approx(ellipsoid_results['iy']) == 67858.40132
    assert pytest.approx(ellipsoid_results['iz']) == 176431.8434
    assert pytest.approx(ellipsoid_results['s']) == 48.936625


def test_elliptical_hemispheroid():
    r = 1
    h = 2
    rho = 2700

    elliptical_hemispheroid_results = sl.elliptical_hemispheroid(r=r, h=h,
                                                                 rho=rho)

    assert pytest.approx(elliptical_hemispheroid_results['m']) == 11309.73355
    assert pytest.approx(elliptical_hemispheroid_results['ix']) == 4948.008429
    assert pytest.approx(elliptical_hemispheroid_results['iy']) == 4948.008429
    assert pytest.approx(elliptical_hemispheroid_results['iz']) == 4523.89342
    assert pytest.approx(elliptical_hemispheroid_results['s']) == 13.8714


def test_revolutioned_paraboloid():
    r = 1
    h = 2
    rho = 2700

    revolutioned_paraboloid_results = sl.revolutioned_paraboloid(r=r, h=h,
                                                                 rho=rho)

    assert pytest.approx(revolutioned_paraboloid_results['m']) == 8482.300165
    assert pytest.approx(revolutioned_paraboloid_results['ix']) == 3298.672286
    assert pytest.approx(revolutioned_paraboloid_results['iy']) == 3298.672286
    assert pytest.approx(revolutioned_paraboloid_results['iz']) == 2827.433388
    assert pytest.approx(revolutioned_paraboloid_results['s']) == 12.18581845


def test_rectangular_pyramid():
    a = 1
    b = 1
    h = 1
    rho = 2700

    rectangular_pyramid_results = sl.rectangular_pyramid(a=a, b=b, h=h,
                                                         rho=rho)

    assert pytest.approx(rectangular_pyramid_results['m']) == 900
    assert pytest.approx(rectangular_pyramid_results['ix']) == 78.75
    assert pytest.approx(rectangular_pyramid_results['iy']) == 78.75
    assert pytest.approx(rectangular_pyramid_results['iz']) == 90
    assert pytest.approx(rectangular_pyramid_results['s']) == 3.236067977


def test_right_angled_wedge():
    a = 1
    b = 1
    h = 1
    rho = 2700

    right_angled_wedge_results = sl.right_angled_wedge(a=a, b=b, h=h, rho=rho)

    assert pytest.approx(right_angled_wedge_results['m']) == 1350
    assert pytest.approx(right_angled_wedge_results['ix']) == 187.5
    assert pytest.approx(right_angled_wedge_results['iy']) == 150
    assert pytest.approx(right_angled_wedge_results['iz']) == 187.5
    assert pytest.approx(right_angled_wedge_results['s']) == 4.414213562


def test_circular_cone():
    r = 1
    h = 1
    rho = 2700

    circular_cone_results = sl.circular_cone(r=r, h=h, rho=rho)

    assert pytest.approx(circular_cone_results['m']) == 2827.433388
    assert pytest.approx(circular_cone_results['ix']) == 530.1437603
    assert pytest.approx(circular_cone_results['iy']) == 530.1437603
    assert pytest.approx(circular_cone_results['iz']) == 848.2300164
    assert pytest.approx(circular_cone_results['s']) == 7.584475592


def test_frustum_cone():
    R = 2
    r = 1
    h = 1
    rho = 2700

    frustum_cone_results = sl.frustum_cone(R=R, r=r, h=h, rho=rho)

    assert pytest.approx(frustum_cone_results['m']) == 19792.03372
    assert pytest.approx(frustum_cone_results['ix']) == 14616.82082
    assert pytest.approx(frustum_cone_results['iy']) == 14616.82082
    assert pytest.approx(frustum_cone_results['iz']) == 26295.13051
    assert pytest.approx(frustum_cone_results['s']) == 25.13274123


def test_spherical_sector():
    r = 1
    h = 1
    rho = 2700

    sperical_sector_results = sl.spherical_sector(r=r, h=h, rho=rho)

    assert pytest.approx(sperical_sector_results['m']) == 5654.866776
    assert pytest.approx(sperical_sector_results['iz']) == 2261.94671


def test_spherical_segment():
    r = 1
    h = 1
    rho = 2700

    spherical_segment_results = sl.spherical_segment(r=r, h=h, rho=rho)

    assert pytest.approx(spherical_segment_results['m']) == 5654.866776
    assert pytest.approx(spherical_segment_results['iz']) == 2261.946711
    assert pytest.approx(spherical_segment_results['s']) == 6.283185307


def test_elliptic_paraboloid():
    a = 1
    b = 1
    h = 1
    rho = 2700

    elliptic_paraboloid_results = sl.elliptic_paraboloid(a=a, b=b, h=h,
                                                         rho=rho)

    assert pytest.approx(elliptic_paraboloid_results['m']) == 4241.150082
    assert pytest.approx(elliptic_paraboloid_results['ix']) == 942.4777961
    assert pytest.approx(elliptic_paraboloid_results['iy']) == 942.4777961
    assert pytest.approx(elliptic_paraboloid_results['iz']) == 1413.716694


def test_torus():
    R = 2
    r = 1
    rho = 2700

    torus_results = sl.torus(R=R, r=r, rho=rho)

    assert pytest.approx(torus_results['m']) == 106591.7275
    assert pytest.approx(torus_results['ix']) == 279803.2848
    assert pytest.approx(torus_results['iy']) == 506310.7056
    assert pytest.approx(torus_results['iz']) == 506310.7056
    assert pytest.approx(torus_results['s']) == 78.95683521


def test_outer_half_torus():
    R = 2
    r = 1
    rho = 2700

    outer_half_torus_results = sl.outer_half_torus(R=R, r=r, rho=rho)

    assert pytest.approx(outer_half_torus_results['m']) == 64605.59732
    assert pytest.approx(outer_half_torus_results['ix']) == 393366.7941
    assert pytest.approx(outer_half_torus_results['iy']) == 1925494.473
    assert pytest.approx(outer_half_torus_results['iz']) == 1925494.473
    assert pytest.approx(outer_half_torus_results['s']) == 52.04478822


def test_prismoid():
    b0 = 3
    b1 = 1
    c0 = 3
    c1 = 1
    h = 2
    rho = 2700

    prismoid_results = sl.prismoid(b0=b0, b1=b1, c0=c0, c1=c1, h=h, rho=rho)

    assert pytest.approx(prismoid_results['A0']) == 9
    assert pytest.approx(prismoid_results['A1']) == 1
    assert pytest.approx(prismoid_results['Am']) == 4
    assert pytest.approx(prismoid_results['x']) == 9/13
    assert pytest.approx(prismoid_results['v']) == 26/3
    assert pytest.approx(prismoid_results['i0x']) == 6.75
    assert pytest.approx(prismoid_results['i0y']) == 6.75
    assert pytest.approx(prismoid_results['i0z']) == 4.313609467
    assert pytest.approx(prismoid_results['i1x']) == 1/12
    assert pytest.approx(prismoid_results['i1y']) == 1/12
    assert pytest.approx(prismoid_results['i1z']) == 1.710059172
    assert pytest.approx(prismoid_results['imx']) == 4/3
    assert pytest.approx(prismoid_results['imy']) == 4/3
    assert pytest.approx(prismoid_results['imz']) == 0.3786982249
    assert pytest.approx(prismoid_results['ix']) == 17154.61538
    assert pytest.approx(prismoid_results['iy']) == 17154.61538
    assert pytest.approx(prismoid_results['iz']) == 6334.43787
