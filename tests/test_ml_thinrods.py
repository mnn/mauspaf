import pytest
# import signal
import mauspaf.masslibrary.thinrods as tr


def test_circular_rod():
    r = 1
    R = 2
    rho = 2700

    cicular_rod_results = tr.circular_rod(r=r, R=R, rho=rho)

    assert pytest.approx(cicular_rod_results['m']) == 106591.7275
    assert pytest.approx(cicular_rod_results['ix']) == 213183.455
    assert pytest.approx(cicular_rod_results['iy']) == 213183.455
    assert pytest.approx(cicular_rod_results['ip']) == 426366.91
    assert pytest.approx(cicular_rod_results['s']) == 78.95683521


def test_semi_circular_rod():
    r = 1
    R = 2
    rho = 2700

    semi_cicular_rod_results = tr.semi_circular_rod(r=r, R=R, rho=rho)

    assert pytest.approx(semi_cicular_rod_results['m']) == 53295.86377
    assert pytest.approx(semi_cicular_rod_results['ix']) == 20188.4732
    assert pytest.approx(semi_cicular_rod_results['iy']) == 106591.7275
    assert pytest.approx(semi_cicular_rod_results['ip']) == 126780.2007
    assert pytest.approx(semi_cicular_rod_results['s']) == 39.47841761


def test_segment_circular_rod():
    r = 1
    R = 2
    alpha = 0.7853981634
    rho = 2700

    segment_cicular_rod_results = tr.segment_circular_rod(r=r, R=R, rho=rho,
                                                          alpha=alpha)

    assert pytest.approx(segment_cicular_rod_results['m']) == 26647.93188
    assert pytest.approx(segment_cicular_rod_results['ix']) == 19366.6631
    assert pytest.approx(segment_cicular_rod_results['iy']) == 825.0644246
    assert pytest.approx(segment_cicular_rod_results['ip']) == 20191.72753
    assert pytest.approx(segment_cicular_rod_results['s']) == 19.7392088


def test_straight_rod():
    # r = 1
    le = 10
    rho = 2700
    iy = 706858.3471

    straight_rod_results = tr.straight_rod(iy=iy, le=le, rho=rho)

    assert pytest.approx(straight_rod_results['m']) == 84823.00165
    assert pytest.approx(straight_rod_results['ix']) == 0.
    assert pytest.approx(abs(straight_rod_results['r'])) == 1.
    assert pytest.approx(straight_rod_results['ip']) == 706858.3471
    assert pytest.approx(abs(straight_rod_results['s'])) == 62.83185307


def test_elliptic_rod():
    a = 1
    b = 2
    r = 0.1
    rho = 2700

    elliptic_rod_results = tr.elliptic_rod(r=r, a=a, b=b, rho=rho)

    assert pytest.approx(elliptic_rod_results['m']) == 707.8356906
    assert pytest.approx(elliptic_rod_results['ix']) == 1315.741637
    assert pytest.approx(elliptic_rod_results['iy']) == 378.9002814
    assert pytest.approx(elliptic_rod_results['ip']) == 1694.641918
    assert pytest.approx(elliptic_rod_results['s']) == 5.243227338


# PARABOLIC ROD: TOO SLOW!!
#
# def test_parabolic_rod():
#     a = 1
#     b = 2
#     r = 0.1
#     rho = 2700

#     signal.alarm(10)
#     parabolic_rod_results = tr.parabolic_rod(a=a, b=b, r=r, rho=rho)

#     assert pytest.approx(parabolic_rod_results['m']) == 389.4371851
#     assert pytest.approx(parabolic_rod_results['ix']) == 1315.741637
#     assert pytest.approx(parabolic_rod_results['iy']) == 378.9002814
#     assert pytest.approx(parabolic_rod_results['ip']) == 1694.641918
#     assert pytest.approx(parabolic_rod_results['s']) == 5.243227338


def test_u_rod():
    # l1 = 10
    l2 = 10
    r = 1
    rho = 2700
    iy = 2827433.38823081

    u_rod_results = tr.u_rod(r=r, iy=iy, l2=l2, rho=rho)

    assert pytest.approx(u_rod_results['m']) == 254469.0049
    assert pytest.approx(u_rod_results['ix']) == 4948008.429
    assert pytest.approx(u_rod_results['l1']) == 10
    assert pytest.approx(u_rod_results['ip']) == 7775441.817
    assert pytest.approx(u_rod_results['s']) == 188.4955592


def test_rectangular_rod():
    l1 = 10
    l2 = 10
    r = 0.1
    rho = 2700

    rectangular_rod_results = tr.rectangular_rod(r=r, l1=l1, l2=l2, rho=rho)

    assert pytest.approx(rectangular_rod_results['m']) == 3392.920066
    assert pytest.approx(rectangular_rod_results['ix']) == 56548.66777
    assert pytest.approx(rectangular_rod_results['iy']) == 56548.66777
    assert pytest.approx(rectangular_rod_results['ip']) == 113097.3355
    assert pytest.approx(rectangular_rod_results['s']) == 25.13274123


def test_v_rod():
    l1 = 10
    r = 0.1
    rho = 2700
    alpha = 0.7853981634

    v_rod_results = tr.v_rod(r=r, l1=l1, alpha=alpha, rho=rho)

    assert pytest.approx(v_rod_results['m']) == 1696.460033
    assert pytest.approx(v_rod_results['ix']) == 28274.33388
    assert pytest.approx(v_rod_results['iy']) == 7068.583471
    assert pytest.approx(v_rod_results['ip']) == 35342.91735
    assert pytest.approx(v_rod_results['s']) == 12.56637061


def test_l_rod():
    l1 = 10
    l2 = 10
    r = 0.1
    rho = 2700

    l_rod_results = tr.l_rod(r=r, l1=l1, l2=l2, rho=rho)

    assert pytest.approx(l_rod_results['m']) == 1696.460033
    assert pytest.approx(l_rod_results['ix']) == 17671.45868
    assert pytest.approx(l_rod_results['iy']) == 17671.45868
    assert pytest.approx(l_rod_results['ip']) == 35342.91735
    assert pytest.approx(l_rod_results['s']) == 12.56637061
