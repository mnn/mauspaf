import numpy as np
from copy import deepcopy
from uncertain import UncertainValue

from mauspaf.core.mauspaf import MassElement, MassElement3D, MassTree
from mauspaf.core.mauspaf import MassObject3D, MassObjectTree


# Mass element definition
aircraft = MassElement(None, None, 'acft')

fuselage_mass = UncertainValue(500, 400, 600, 'normal', [500, 30])
fuselage_cg = UncertainValue(3.1, 2, 4.5, 'normal', [3, 0.5])
fuselage = MassElement(fuselage_mass, fuselage_cg, 'f')

wing_mass = UncertainValue(130, 60, 200, 'normal', [120, 20])
wing_cg = UncertainValue(2.7, 2, 3, 'normal', [2.7, 0.2])
wing = MassElement(wing_mass, wing_cg, 'w')

seat_mass = UncertainValue(10, 5, 15)
seat_cg = UncertainValue(2.5, 2.1, 2.8)
seat = MassElement(seat_mass, seat_cg)

spar_mass = UncertainValue(10, 5, 15)
spar_cg = UncertainValue(2.6, 2.1, 2.8)
spar = MassElement(spar_mass, spar_cg, 'sp')


# Mass tree definition
main_mass_tree = MassTree(
    {x.id: x for x in [aircraft, fuselage, wing, seat, spar]},
    {'f': 'acft', 'w': 'acft', seat.id: 'f', 'sp': 'w'})


# Testing functions
def test_types():
    # Tests if id sets are correctly defined
    assert main_mass_tree.nodes == {seat.id, 'w', 'f', 'acft', 'sp'}
    assert main_mass_tree.leafs == {seat.id, 'sp'}
    assert main_mass_tree.branch_nodes == {'f', 'acft', 'w'}
    assert main_mass_tree.root == {'acft'}


def test_masselement3d():
    a = MassElement3D(6, 2, 2, 2, 1, 1, 1, -1, -1, -1)
    b = a + 0
    d = 0 + a
    c = b + d
    assert c.mass == 12
    assert c.x == 2
    assert c.y == 2
    assert c.z == 2
    assert c.ixx == 2
    assert c.iyy == 2
    assert c.izz == 2
    assert c.ixy == -2
    assert c.iyz == -2
    assert c.izx == -2


def test_massobject3d():
    a = MassElement3D(6, 2, 2, 2, 1, 1, 1, -1, -1, -1)
    b = MassElement3D(6, 3, 3, 3, 1, 1, 1, -1, -1, -1)
    c = MassElement3D(6, 4, 4, 4, 1, 1, 1, -1, -1, -1)

    mass_object = MassObject3D(estimated=a, calculated=b)
    assert mass_object.measured is None
    mass_object.measured = c
    assert mass_object.estimated.x == 2
    assert mass_object.calculated.x == 3


def test_massobjecttree():
    seat1 = MassObject3D(estimated=MassElement3D(mass=10),
                         measured=MassElement3D(mass=20),
                         id='s01')
    seat2 = MassObject3D(estimated=MassElement3D(mass=10), id='s02')
    seats = MassObject3D(active='calculated', id='s00')
    furniture = MassObject3D(estimated=MassElement3D(mass=10))
    cabin = MassObject3D(active='calculated')
    cabin_tree = MassObjectTree(
        elements_list=[seat1, seat2, seats, furniture, cabin],
        parent_dict={'s01': 's00', 's02': 's00', 's00': cabin.id,
                     furniture.id: cabin.id})

    assert cabin_tree.elements[cabin.id].active_properties().mass == 30

    seat1.active = 'measured'
    cabin_tree.update()

    assert cabin_tree.elements[cabin.id].active_properties().mass == 40


def test_masselement3d_rotation():
    a = MassElement3D(6, 2, 2, 2, 2, 1, 1)
    b = deepcopy(a)
    assert a.ixx == b.ixx

    a.rotate(axis=[0, 0, 1], angle=np.pi/6)
    b.rotate(axis=[0, 0, 1], angle=UncertainValue(5, 3, 7, 'normal',
                                                  [5, 0.5])*np.pi/180)
