import pytest
import mauspaf.masslibrary.shells as sh


def test_lateral_cylindrical_shell():
    r = 1
    h = 1
    t = 0.1
    rho = 2700

    lateral_cylindrical_shell_results = sh.lateral_cylindrical_shell(r=r, h=h,
                                                                     t=t,
                                                                     rho=rho)

    assert pytest.approx(lateral_cylindrical_shell_results['m']) == 1696.46
    assert pytest.approx(lateral_cylindrical_shell_results['ix']) == 989.601666
    assert pytest.approx(lateral_cylindrical_shell_results['iy']) == 989.601666
    assert pytest.approx(lateral_cylindrical_shell_results['iz']) == 1696.46
    assert pytest.approx(lateral_cylindrical_shell_results['s']) == 6.283185


def test_total_cylindrical_shell():
    r = 1
    h = 1
    t = 0.1
    rho = 2700

    total_cylindrical_shell_results = sh.total_cylindrical_shell(r=r, h=h, t=t,
                                                                 rho=rho)

    assert pytest.approx(total_cylindrical_shell_results['m']) == 3392.920066
    assert pytest.approx(total_cylindrical_shell_results['ix']) == 1837.831702
    assert pytest.approx(total_cylindrical_shell_results['iy']) == 1837.831702
    assert pytest.approx(total_cylindrical_shell_results['iz']) == 2544.69
    assert pytest.approx(total_cylindrical_shell_results['s']) == 12.56637061


def test_elliptical_shell():
    a = 1
    b = 2
    h = 1
    t = 0.1
    rho = 2700

    elliptical_shell_results = sh.elliptical_shell(a=a, b=b, h=h,
                                                   t=t, rho=rho)

    assert pytest.approx(elliptical_shell_results['m']) == 6075.258898
    assert pytest.approx(elliptical_shell_results['ix']) == 10053.10699
    assert pytest.approx(elliptical_shell_results['iy']) == 6798.504005
    assert pytest.approx(elliptical_shell_results['iz']) == 15839.06784
    assert pytest.approx(elliptical_shell_results['s']) == 22.50095888


def test_hollow_box():
    a = 1
    b = 1
    c = 1
    t = 0.1
    rho = 2700

    hollow_box_results = sh.hollow_box(a=a, b=b, c=c, t=t, rho=rho)

    assert pytest.approx(hollow_box_results['m']) == 1620
    assert pytest.approx(hollow_box_results['ix']) == 450
    assert pytest.approx(hollow_box_results['iy']) == 450
    assert pytest.approx(hollow_box_results['iz']) == 450
    assert pytest.approx(hollow_box_results['s']) == 6


def test_open_hollow_box():
    a = 1
    b = 1
    c = 1
    t = 0.1
    rho = 2700

    open_hollow_box_results = sh.open_hollow_box(a=a, b=b, c=c, t=t, rho=rho)

    assert pytest.approx(open_hollow_box_results['m']) == 1080
    assert pytest.approx(open_hollow_box_results['ix']) == 270
    assert pytest.approx(open_hollow_box_results['iy']) == 180
    assert pytest.approx(open_hollow_box_results['iz']) == 270
    assert pytest.approx(open_hollow_box_results['s']) == 4


def test_frustum_cone_shell():
    R = 2
    r = 1
    h = 1
    t = 0.1
    rho = 2700

    frustum_cone_shell_results = sh.frustum_cone_shell(R=R, r=r, h=h, t=t,
                                                       rho=rho)

    assert pytest.approx(frustum_cone_shell_results['m']) == 3598.73518
    assert pytest.approx(frustum_cone_shell_results['ix']) == 4787.206366
    assert pytest.approx(frustum_cone_shell_results['iy']) == 4787.206366
    assert pytest.approx(frustum_cone_shell_results['iz']) == 8996.83795
    assert pytest.approx(frustum_cone_shell_results['s']) == 13.32864881


def circular_cone_shell():
    r = 1
    h = 1
    t = 0.1
    rho = 2700

    circular_cone_shell_results = sh.circular_cone_shell(r=r, h=h, t=t,
                                                         rho=rho)

    assert pytest.approx(circular_cone_shell_results['m']) == 1199.578393
    assert pytest.approx(circular_cone_shell_results['ix']) == 366.5378424
    assert pytest.approx(circular_cone_shell_results['iy']) == 366.5378424
    assert pytest.approx(circular_cone_shell_results['iz']) == 599.7891965
    assert pytest.approx(circular_cone_shell_results['s']) == 4.442882938


def test_spherical_shell():
    r = 1
    t = 0.1
    rho = 2700

    spherical_shell_results = sh.spherical_shell(r=r, t=t, rho=rho)

    assert pytest.approx(spherical_shell_results['m']) == 3392.920066
    assert pytest.approx(spherical_shell_results['ix']) == 2261.946711
    assert pytest.approx(spherical_shell_results['iy']) == 2261.946711
    assert pytest.approx(spherical_shell_results['iz']) == 2261.946711
    assert pytest.approx(spherical_shell_results['s']) == 12.56637061


def test_hemispherical_shell():
    r = 1
    t = 0.1
    rho = 2700

    hemispherical_shell_results = sh.hemispherical_shell(r=r, t=t, rho=rho)

    assert pytest.approx(hemispherical_shell_results['m']) == 1696.460033
    assert pytest.approx(hemispherical_shell_results['ix']) == 706.8583471
    assert pytest.approx(hemispherical_shell_results['iy']) == 706.8583471
    assert pytest.approx(hemispherical_shell_results['iz']) == 1130.973355
    assert pytest.approx(hemispherical_shell_results['s']) == 6.283185307


def test_elliptical_hemispheriodal_shell():
    r = 2
    h = 1
    t = 0.1
    rho = 2700
    axis = 'minor'

    hemispheriodal_shell_results = sh.elliptical_hemispheriodal_shell(
        r=r, h=h, t=t, rho=rho, axis=axis)

    assert pytest.approx(hemispheriodal_shell_results['m']) == 4682.81666
    assert pytest.approx(hemispheriodal_shell_results['s']) == 17.34376541
    assert pytest.approx(hemispheriodal_shell_results['e']) == 0.8660254038


def test_revolutioned_paraboloid_shell():
    r = 2
    h = 1
    t = 0.1
    rho = 2700

    paraboloid_shell_results = sh.revolutioned_paraboloid_shell(r=r, h=h, t=t,
                                                                rho=rho)

    assert pytest.approx(paraboloid_shell_results['m']) == 4135.80472
    assert pytest.approx(paraboloid_shell_results['ix']) == 4708.707019
    assert pytest.approx(paraboloid_shell_results['iy']) == 4708.707019
    assert pytest.approx(paraboloid_shell_results['iz']) == 8737.315881
    assert pytest.approx(paraboloid_shell_results['s']) == 15.31779526
    assert pytest.approx(paraboloid_shell_results['P']) == 22.627417


def test_hollow_torus_sector():
    R = 1
    ro = 0.2
    ri = 0.1
    alpha = 0.7853981634
    rho = 2700

    hollow_torus_sector_results = sh.hollow_torus_sector(R=R, ro=ro, ri=ri,
                                                         alpha=alpha, rho=rho)

    assert pytest.approx(hollow_torus_sector_results['m']) == 399.7189782
    assert pytest.approx(hollow_torus_sector_results['ix']) == 80.34491087
    assert pytest.approx(hollow_torus_sector_results['iy']) == 12.20587851
    assert pytest.approx(hollow_torus_sector_results['iz']) == 82.55781493
    assert pytest.approx(hollow_torus_sector_results['s']) == 1.97392088
