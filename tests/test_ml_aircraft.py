import pytest
import numpy as np

import mauspaf.masslibrary.aircraft as ac


def test_ga_wing_raymer():
    ar = 7
    mdg = 1800
    mf = 100
    nz = 3.8*1.5
    phi = 0
    qc = 1
    s = 10
    tc = 0.18
    tr = 0.6

    mw = ac.ga_wing_raymer(ar=ar, mdg=mdg, mf=mf, nz=nz, phi=phi, qc=qc, s=s,
                           tc=tc, tr=tr)['mw']
    mw_lb = ac.ga_wing_raymer(ar=ar, mdg=mdg, mf=mf, nz=nz, phi=phi, qc=qc,
                              s=s, tc=tc, tr=tr, units={'mw': 'lb'})['mw']

    assert pytest.approx(mw) == 223.8492909/2.204623
    assert pytest.approx(mw_lb) == 223.8492909

    # Units
    w1 = ac.ga_wing_raymer(ar=ar, mdg=mdg, mf=mf, nz=nz, phi=phi, qc=qc, s=s,
                           tc=tc, tr=tr, units={'mw': 'lb'})
    assert 'units' in w1
    assert w1['units']['mw'] == 'lb'
    assert w1['units']['s'] == 'm²'


def test_ga_wing_nicolai():
    ar = 7
    mdg = 1800
    nz = 3.8*1.5
    phi = 0
    s = 10
    tc = 0.18
    tr = 0.6
    vh = 100

    mw = ac.ga_wing_nicolai(ar=ar, mdg=mdg, nz=nz, phi=phi, s=s, tc=tc, tr=tr,
                            vh=vh)['mw']
    mw_lb = ac.ga_wing_nicolai(ar=ar, mdg=mdg, nz=nz, phi=phi, s=s, tc=tc,
                               tr=tr, vh=vh, units={'mw': 'lb'})['mw']

    mw2 = ac.ga_wing_nicolai(
        ar=ar, mdg=mdg, nz=nz, phi=phi, s=s*1e4, tc=tc, tr=tr, vh=vh,
        units={'s': 'cm²'})['mw']

    assert pytest.approx(mw, rel=1e-3) == 143.34
    assert pytest.approx(mw_lb, rel=1e-3) == 143.34*2.204623
    assert pytest.approx(mw) == mw2


def test_ga_horizontal_tail_raymer():
    ar = 7
    mdg = 1800
    nz = 3.8*1.5
    phi = 0
    qc = 1
    s = 10
    tc = 0.18
    tr = 0.6

    mht = ac.ga_horizontal_tail_raymer(
        ar=ar, mdg=mdg, nz=nz, phi=phi, qc=qc, s=s, tc=tc, tr=tr)['mht']
    mht_lb = ac.ga_horizontal_tail_raymer(
        ar=ar, mdg=mdg, nz=nz, phi=phi, qc=qc, s=s, tc=tc, tr=tr,
        units={'mht': 'lb'})['mht']

    assert pytest.approx(mht) == 27.25196516/2.204623
    assert pytest.approx(mht_lb) == 27.25196516


def test_ga_horizontal_tail_nicolai():
    bht = 4
    lht = 10
    mdg = 1800
    nz = 3.8*1.5
    s = 10
    tht = 5

    mht = ac.ga_horizontal_tail_nicolai(
        bht=bht, lht=lht, mdg=mdg, nz=nz, s=s, tht=tht)['mht']
    mht_lb = ac.ga_horizontal_tail_nicolai(
        bht=bht, lht=lht, mdg=mdg, nz=nz, s=s, tht=tht,
        units={'mht': 'lb'})['mht']

    assert pytest.approx(mht) == 23.20839729
    assert pytest.approx(mht_lb) == 23.20839729*2.204623


def test_vertical_tail_raymer():
    ar = 7
    F = 1
    mdg = 1800
    nz = 3.8*1.5
    phi = 0
    qc = 1
    s = 10
    tc = 0.18
    tr = 0.6

    mvt = ac.ga_vertical_tail_raymer(
        ar=ar, F=F, mdg=mdg, nz=nz, phi=phi, qc=qc, s=s, tc=tc, tr=tr)['mvt']
    mvt_lb = ac.ga_vertical_tail_raymer(
        ar=ar, F=F, mdg=mdg, nz=nz, phi=phi, qc=qc, s=s, tc=tc, tr=tr,
        units={'mvt': 'lb'})['mvt']

    assert pytest.approx(mvt) == 67.09411199/2.204623
    assert pytest.approx(mvt_lb) == 67.09411199


def test_vertical_tail_nicolai():
    bvt = 4
    mdg = 1800
    nz = 3.8*1.5
    s = 10
    tvt = 5

    mvt = ac.ga_vertical_tail_nicolai(bvt=bvt, mdg=mdg, nz=nz, s=s,
                                      tvt=tvt)['mvt']
    mvt_lb = ac.ga_vertical_tail_nicolai(bvt=bvt, mdg=mdg, nz=nz, s=s, tvt=tvt,
                                         units={'mvt': 'lb'})['mvt']

    assert pytest.approx(mvt) == 7.623489/2.204623
    assert pytest.approx(mvt_lb) == 7.623489


def test_fuselage_raymer():
    dp = 1
    dfs = 4
    lfs = 10
    lht = 10
    mdg = 1800
    nz = 3.8*1.5
    s = 10
    qc = 1
    vp = 10

    mfs = ac.ga_fuselage_raymer(dp=dp, dfs=dfs, lfs=lfs, lht=lht, mdg=mdg,
                                nz=nz, s=s, qc=qc, vp=vp)['mfs']
    mfs_lb = ac.ga_fuselage_raymer(dp=dp, dfs=dfs, lfs=lfs, lht=lht, mdg=mdg,
                                   nz=nz, s=s, qc=qc, vp=vp,
                                   units={'mfs': 'lb'})['mfs']

    assert pytest.approx(mfs) == 20.54453222/2.204623
    assert pytest.approx(mfs_lb) == 20.54453222


def test_fuselage_nicolai():
    df = 4
    lf = 10
    mdg = 1800
    nz = 3.8*1.5
    vh = 100
    wf = 4

    mfs = ac.ga_fuselage_nicolai(df=df, lf=lf, mdg=mdg, nz=nz, vh=vh,
                                 wf=wf)['mfs']
    mfs_lb = ac.ga_fuselage_nicolai(df=df, lf=lf, mdg=mdg, nz=nz, vh=vh, wf=wf,
                                    units={'mfs': 'lb'})['mfs']

    assert pytest.approx(mfs, rel=1e-3) == 644.68
    assert pytest.approx(mfs_lb, rel=1e-3) == 644.68*2.204623


def test_main_landing_gear_raymer():
    lm = 4
    ml = 1800
    nl = 3.8*1.5

    mlg = ac.ga_main_landing_gear_raymer(lm=lm, ml=ml, nl=nl)['mlg']
    mlg_lb = ac.ga_main_landing_gear_raymer(lm=lm, ml=ml, nl=nl,
                                            units={'mlg': 'lb'})['mlg']

    assert pytest.approx(mlg) == 217.7012083/2.204623
    assert pytest.approx(mlg_lb) == 217.7012083


def test_main_landing_gear_nicolai():
    lm = 5
    ml = 1800
    nl = 3.8*1.5

    mmnlg = ac.ga_main_landing_gear_nicolai(lm=lm, ml=ml, nl=nl)['mmnlg']
    mmnlg_lb = ac.ga_main_landing_gear_nicolai(lm=lm, ml=ml, nl=nl,
                                               units={'mmnlg': 'lb'})['mmnlg']

    assert pytest.approx(mmnlg) == 276.0878072/2.204623
    assert pytest.approx(mmnlg_lb) == 276.0878072


def test_nose_landing_gear_raymer():
    l_n = 5
    ml = 1800
    nl = 3.8*1.5

    mnlg = ac.ga_nose_landing_gear_raymer(l_n=l_n, ml=ml, nl=nl)['mnlg']
    mnlg_lb = ac.ga_nose_landing_gear_raymer(l_n=l_n, ml=ml, nl=nl,
                                             units={'mnlg': 'lb'})['mnlg']

    assert pytest.approx(mnlg) == 387.4214607/2.204623
    assert pytest.approx(mnlg_lb) == 387.4214607


def test_installed_engine_raymer():
    meng = 1800
    neng = 2

    mei = ac.ga_installed_engine_raymer(meng=meng, neng=neng)['mei']
    mei_lb = ac.ga_installed_engine_raymer(meng=meng, neng=neng,
                                           units={'mei': 'lb'})['mei']

    assert pytest.approx(mei) == 10708.35437/2.204623
    assert pytest.approx(mei_lb) == 10708.35437


def test_installed_engine_nicolai():
    meng = 1800
    neng = 2

    mei = ac.ga_installed_engine_nicolai(meng=meng, neng=neng)['mei']
    mei_lb = ac.ga_installed_engine_nicolai(meng=meng, neng=neng,
                                            units={'mei': 'lb'})['mei']

    assert pytest.approx(mei) == 10708.35437/2.204623
    assert pytest.approx(mei_lb) == 10708.35437


def test_fuel_system_raymer():
    neng = 2
    ntank = 2
    qint = 10
    qtot = 10

    mfsy = ac.ga_fuel_system_raymer(neng=neng, ntank=ntank, qint=qint,
                                    qtot=qtot)['mfsy']
    mfsy_lb = ac.ga_fuel_system_raymer(neng=neng, ntank=ntank, qint=qint,
                                       qtot=qtot, units={'mfsy': 'lb'})['mfsy']

    assert pytest.approx(mfsy) == 778.6212142/2.204623
    assert pytest.approx(mfsy_lb) == 778.6212142


def test_fuel_system_nicolai():
    neng = 2
    ntank = 2
    qint = 10
    qtot = 10

    mfsy = ac.ga_fuel_system_nicolai(
        neng=neng, ntank=ntank, qint=qint, qtot=qtot)['mfsy']
    mfsy_lb = ac.ga_fuel_system_nicolai(
        neng=neng, ntank=ntank, qint=qint, qtot=qtot,
        units={'mfsy': 'lb'})['mfsy']

    assert pytest.approx(mfsy) == 778.7831407/2.204623
    assert pytest.approx(mfsy_lb) == 778.7831407


def test_flight_control_system_raymer():
    b = 10
    lfs = 10
    mdg = 1800
    nz = 3.8*1.5

    mctrl = ac.ga_flight_control_system_raymer(b=b, lfs=lfs, mdg=mdg,
                                               nz=nz)['mctrl']
    mctrl_lb = ac.ga_flight_control_system_raymer(
        b=b, lfs=lfs, mdg=mdg, nz=nz, units={'mctrl': 'lb'})['mctrl']

    assert pytest.approx(mctrl) == 79.22180224/2.204623
    assert pytest.approx(mctrl_lb) == 79.22180224


def test_flight_control_system_nicolai():
    mdg = 1800
    ctrlsys = 'manual'

    mctrl = ac.ga_flight_control_system_nicolai(
        mdg=mdg, ctrlsys=ctrlsys)['mctrl']
    mctrl_lb = ac.ga_flight_control_system_nicolai(
        mdg=mdg, ctrlsys=ctrlsys, units={'mctrl': 'lb'})['mctrl']

    assert pytest.approx(mctrl) == 190.7590214/2.204623
    assert pytest.approx(mctrl_lb) == 190.7590214


def test_hydraulic_system_raymer():
    mdg = 1800

    mhyd = ac.ga_hydraulic_system_raymer(mdg=mdg)['mhyd']
    mhyd_lb = ac.ga_hydraulic_system_raymer(mdg=mdg,
                                            units={'mhyd': 'lb'})['mhyd']

    assert pytest.approx(mhyd) == 3.9683214/2.204623
    assert pytest.approx(mhyd_lb) == 3.9683214


def test_avionics_system_raymer():
    muav = 1800

    mav = ac.ga_avionics_system_raymer(muav=muav)['mav']
    mav_lb = ac.ga_avionics_system_raymer(muav=muav,
                                          units={'mav': 'lb'})['mav']

    assert pytest.approx(mav) == 4821.931526/2.204623
    assert pytest.approx(mav_lb) == 4821.931526


def test_avionics_system_nicolai():
    muav = 1800

    mav = ac.ga_avionics_system_nicolai(muav=muav)['mav']
    mav_lb = ac.ga_avionics_system_nicolai(muav=muav,
                                           units={'mav': 'lb'})['mav']

    assert pytest.approx(mav) == 4821.931526/2.204623
    assert pytest.approx(mav_lb) == 4821.931526


def test_electrical_system_raymer():
    mav = 1800
    mfsy = 1800

    mel = ac.ga_electrical_system_raymer(mav=mav, mfsy=mfsy)['mel']
    mel_lb = ac.ga_electrical_system_raymer(mav=mav, mfsy=mfsy,
                                            units={'mel': 'lb'})['mel']

    assert pytest.approx(mel) == 1225.039419/2.204623
    assert pytest.approx(mel_lb) == 1225.039419


def test_electrical_system_nicolai():
    mav = 1800
    mfsy = 1800

    mel = ac.ga_electrical_system_nicolai(mav=mav, mfsy=mfsy)['mel']
    mel_lb = ac.ga_electrical_system_nicolai(mav=mav, mfsy=mfsy,
                                             units={'mel': 'lb'})['mel']

    assert pytest.approx(mel) == 1225.039419/2.204623
    assert pytest.approx(mel_lb) == 1225.039419


def test_air_conditioning_anti_ice_raymer():
    mach = 0.8
    mav = 1800
    mdg = 1800
    nocc = 4

    mac = ac.ga_air_conditioning_anti_ice_raymer(mach=mach, mav=mav, mdg=mdg,
                                                 nocc=nocc)['mac']
    mac_lb = ac.ga_air_conditioning_anti_ice_raymer(
        mach=mach, mav=mav, mdg=mdg, nocc=nocc,
        units={'mac': 'lb'})['mac']

    assert pytest.approx(mac) == 203.204156/2.204623
    assert pytest.approx(mac_lb) == 203.204156


def test_air_conditioning_anti_ice_nicolai():
    mach = 0.8
    mav = 1800
    mdg = 1800
    nocc = 4

    mac = ac.ga_air_conditioning_anti_ice_nicolai(mach=mach, mav=mav, mdg=mdg,
                                                  nocc=nocc)['mac']
    mac_lb = ac.ga_air_conditioning_anti_ice_nicolai(
        mach=mach, mav=mav, mdg=mdg, nocc=nocc,
        units={'mac': 'lb'})['mac']

    assert pytest.approx(mac) == 203.204156/2.204623
    assert pytest.approx(mac_lb) == 203.204156


def test_furnishings_raymer():
    mdg = 1800

    mfurn = ac.ga_furnishings_raymer(mdg=mdg)['mfurn']
    mfurn_lb = ac.ga_furnishings_raymer(mdg=mdg,
                                        units={'mfurn': 'lb'})['mfurn']

    assert pytest.approx(mfurn) == 165.9563055/2.204623
    assert pytest.approx(mfurn_lb) == 165.9563055


def test_furnishings_nicolai():
    ncrew = 4
    qh = 1

    mfurn = ac.ga_furnishings_nicolai(ncrew=ncrew, qh=qh)['mfurn']
    mfurn_lb = ac.ga_furnishings_nicolai(ncrew=ncrew, qh=qh,
                                         units={'mfurn': 'lb'})['mfurn']

    assert pytest.approx(mfurn) == 52.46141222/2.204623
    assert pytest.approx(mfurn_lb) == 52.46141222


def test_wing_mass_howe():
    m0 = 1800
    ar = 1
    s = 10
    ni = 1.5
    mzw = 1500
    theta = 0
    phi = 0
    tr = 1
    tc = 1
    conf = 1
    composite = True

    mr = ac.wing_mass_howe(m0=m0, ar=ar, s=s, ni=ni, mzw=mzw, theta=theta,
                           phi=phi, tr=tr, tc=tc, conf=conf,
                           composite=composite)['mr']
    mc = ac.wing_mass_howe(m0=m0, ar=ar, s=s, ni=ni, mzw=mzw, theta=theta,
                           phi=phi, tr=tr, tc=tc, conf=conf,
                           composite=composite)['mc']
    fa = ac.wing_mass_howe(m0=m0, ar=ar, s=s, ni=ni, mzw=mzw, theta=theta,
                           phi=phi, tr=tr, tc=tc, conf=conf,
                           composite=composite)['fa']

    assert pytest.approx(mr) == 0.181059899
    assert pytest.approx(mc) == 135760858933.483
    assert pytest.approx(fa) == 6332506.303


def test_structural_mass_torenbeek():
    mtow = 1800
    nult = 1.5
    bf = 1
    lf = 1
    hf = 1
    composite = True
    ms = ac.structural_mass_torenbeek(
        mtow=mtow, nult=nult, bf=bf, hf=hf, lf=lf, composite=composite)['ms']
    assert pytest.approx(ms) == 138.605091


def test_wing_mass_torenbeek():
    mtow = 3000
    b = 15
    phi = 15*np.pi/180
    s = 20
    tr = 0.5
    nult = 3.8*1.5
    composite = False
    mw = ac.wing_mass_torenbeek(mtow=mtow, b=b, phi=phi, s=s, tr=tr, nult=nult,
                                composite=composite)['mw']
    assert pytest.approx(mw, rel=1e-3) == 252.14


def test_tailplane_mass_torenbeek():
    nult = 1.5
    stail = 10
    composite = True
    mtail = ac.tailplane_mass_torenbeek(
        nult=nult, stail=stail, composite=composite)['mtail']
    assert pytest.approx(mtail) == 23.31671391


def test_fuselage_torenbeek():
    vd = 50
    lt = 10
    bf = 5
    hf = 5
    sg = 50
    composite = True
    mf = ac.fuselage_torenbeek(
        vd=vd, lt=lt, bf=bf, hf=hf, sg=sg, composite=composite)['mf']
    assert pytest.approx(mf) == 151.1456905


def test_landing_gear_torenbeek():
    mtow = 1800
    a = 1
    b = 1
    c = 1
    d = 1
    conf = 'low'
    composite = True
    mlg = ac.landing_gear_torenbeek(
        mtow=mtow, a=a, b=b, c=c, d=d, conf=conf, composite=composite)['mlg']
    assert pytest.approx(mlg) == 66678.14726


def test_surface_controls_torenbeek():
    ksc = 1
    mtow = 1800
    composite = True
    msc = ac.surface_controls_torenbeek(
        ksc=ksc, mtow=mtow, composite=composite)['msc']
    assert pytest.approx(msc) == 125.7768158
