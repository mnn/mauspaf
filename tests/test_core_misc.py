import numpy as np
from mauspaf.core.misc import rodrigues_rotation


def test_rodrigues_rotation():
    rot = rodrigues_rotation([0, 0, 1], np.pi/2)
    assert np.allclose(rot, np.array([[0, -1, 0], [1, 0, 0], [0, 0, 1]]))
