""" MauSPAF: mass properties calculations.

.. module:: mauspaf
.. moduleauthor:: Miguel Nuño <mnunos@outlook.com>

"""
# flake8: noqa
from . import core
from . import masslibrary
from . import ui
from . import widgets
from . import guifun
from . import generated

__version__ = '0.0.4'
