""" Automatically generated windows with QtDesigner and pyuic
"""
# flake8: noqa
from . import aboutwindow
from . import dl_partinfo
from . import dw_maintree
from . import dw_masslibrary
from . import dw_parteditor
from . import dw_plot
from . import mainwindow
