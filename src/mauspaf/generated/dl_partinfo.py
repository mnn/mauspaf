# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'resources/ui/dl_partinfo.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.tl_title = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setUnderline(True)
        self.tl_title.setFont(font)
        self.tl_title.setObjectName("tl_title")
        self.verticalLayout.addWidget(self.tl_title)
        self.tl_mass_title = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.tl_mass_title.setFont(font)
        self.tl_mass_title.setObjectName("tl_mass_title")
        self.verticalLayout.addWidget(self.tl_mass_title)
        self.tl_mass_text = QtWidgets.QLabel(Dialog)
        self.tl_mass_text.setObjectName("tl_mass_text")
        self.verticalLayout.addWidget(self.tl_mass_text)
        self.tl_cg_title = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.tl_cg_title.setFont(font)
        self.tl_cg_title.setObjectName("tl_cg_title")
        self.verticalLayout.addWidget(self.tl_cg_title)
        self.tl_cg_text = QtWidgets.QLabel(Dialog)
        self.tl_cg_text.setObjectName("tl_cg_text")
        self.verticalLayout.addWidget(self.tl_cg_text)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Part - Statistical information"))
        self.tl_title.setText(_translate("Dialog", "Part name"))
        self.tl_mass_title.setText(_translate("Dialog", "Mass"))
        self.tl_mass_text.setText(_translate("Dialog", "TextLabel"))
        self.tl_cg_title.setText(_translate("Dialog", "CG"))
        self.tl_cg_text.setText(_translate("Dialog", "TextLabel"))
