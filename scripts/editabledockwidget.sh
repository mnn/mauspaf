#!/bin/sh
# After creating a new dockwidget with QtDesigner. it doesn't include a
# "dockWidgetContents". Therefore no widgets can be added to the
# dockwidget usig the QtCreator GUI. The solution is to manually add
# some text in the *.ui class. This function adds the text needed.
#
# See also: https://stackoverflow.com/questions/47914013/qdockwidget-form-in-qt-creator-designer
sed -i '/<\/widget>/i \ \ <widget class="QWidget" name="dockWidgetContents"/>' $1
