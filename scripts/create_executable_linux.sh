#!/bin/bash
if [[ `git status --porcelain` ]]; then
    read -p 'There are unstaged changes in the directory. Are you sure you want to generate a new executable? [Y/n]: ' new
    if [ "$new" = 'n' ]; then
	echo "Aborting calcualtion"
	exit 1
    fi
    echo 'Proceeding with creation...'
else
  echo "Everything is staged and commited. Proceeding with creation..."
fi


# Last commit
last_commit=$(git log -n1 --format=format:"%H")

# Expiry date
default_expiry_date=$(date --date='+6 months' +"%Y-%m-%d")
read -p "Input desired expiry date YYYY-MM-DD [$default_expiry_date]:" expiry_date
if [ "$expiry_date" = '' ]; then
    expiry_date=$default_expiry_date
fi

# Write data to config file

