export PROJECTNAME=$(shell basename "$(PWD)")
export PYDIR=$(shell if [ -d "./venv-mauspaf" ]; then echo "./venv-mauspaf/bin/"; else echo ""; fi)


.SILENT: ;         # no need for @

setup: ## Setup virtual environment and install dependencies
	echo "Run the following commands to install required dependencies in a virtual environment"
	echo "> python3 -m venv venv-mauspaf"
	echo "> source venv-mauspaf/bin/activate"
	echo "> pip3 install -r requirements/use.txt"
	echo "> pip3 install -r requirements/dev.txt"
	echo "If the directory ./venv-mauspaf does not exist the system-wide python environment will be used\n"
	echo "\nOnce everything is installed, 'make run' to run the application"

deps: ## Reinstalls dependencies
	${PYDIR}python3 -m pip install -r requirements/use.txt
	${PYDIR}python3 -m pip install -r requirements/dev.txt

doc: ## Regenerates documentation
	echo "Generation of documentation through makefile not yet implemented"

clean: ## Clean package and generated files
	rm -rf build/* dist/*
	rm -rf src/mauspaf/generated/*

package: clean ## Rebuilds venv and packages app
	rm -rf venv-mauspaf
	python3 -m venv venv-mauspaf
	${PYDIR}python3 -m pip install -r requirements/use.txt
	${PYDIR}python3 -m pip install -r requirements/dev.txt
	export PYTHONPATH=`pwd`:$PYTHONPATH && ${PYDIR}python3 setup.py bdist_app

uic: ## Converts ui files to python
	for i in `ls resources/ui/*.ui`; do FNAME=`basename $${i} ".ui"`; ${PYDIR}pyuic5 $${i} > "./src/mauspaf/generated/$${FNAME}.py"; done
	./scripts/removecentralwidget.sh ./src/mauspaf/generated/mainwindow.py

res: ## Generates and compresses resource file
	${PYDIR}pyrcc5 -compress 9 -o src/resources_rc.py resources/resources.qrc

exec: ## Creates single executable file
	pyinstaller mauspaf.spec

all: res uic exec ## res + uic + exec

run: ## Runs the application
	export PYTHONPATH=`pwd`:$PYTHONPATH && python src/mauspaf/application.py

help: makefile
	echo
	echo " Choose a command run in "$(PROJECTNAME)":"
	echo
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	echo
